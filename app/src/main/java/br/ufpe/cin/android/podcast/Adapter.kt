package br.ufpe.cin.android.podcast

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.prof.rssparser.Article

class Adapter(private val lista: MutableList<Article>,
              var onTitleListener: OnClickTitleListener) : RecyclerView.Adapter<Adapter.ViewHolder>() {

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val title: TextView = view.findViewById(R.id.item_title)
        val date: TextView = view.findViewById(R.id.item_date)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.itemfeed, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = lista[position].title
        holder.date.text = lista[position].pubDate

        holder.title.setOnClickListener {
            onTitleListener.onClick(lista[position])
        }
    }

    override fun getItemCount(): Int {
        return lista.size
    }

    // interface vai transmitir responsabilidade para MainActivity: callback
    interface OnClickTitleListener {
        fun onClick(episodio: Article)
    }

}