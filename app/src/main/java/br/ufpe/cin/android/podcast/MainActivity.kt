package br.ufpe.cin.android.podcast

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import br.ufpe.cin.android.podcast.databinding.ActivityMainBinding
import com.prof.rssparser.Article
import com.prof.rssparser.Parser
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity(), Adapter.OnClickTitleListener {
    private lateinit var binding : ActivityMainBinding
    private lateinit var parser : Parser
    private val scope = CoroutineScope(Dispatchers.Main.immediate)
    private lateinit var pref: SharedPreferences

    companion object {
        val PODCAST_FEED = "https://jovemnerd.com.br/feed-nerdcast/"

        val EXTRA_DESCRIPTION = "EPISODE_DESCRIPTION"
        val EXTRA_LINK = "EPISODE_LINK"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        parser = Parser.Builder()
            .context(this)
            .cacheExpirationMillis(24L * 60L * 60L * 100L)
            .build()
    }

    override fun onStart() {
        super.onStart()

        pref = PreferenceManager.getDefaultSharedPreferences(this)

        scope.launch {
            val channel = withContext(Dispatchers.IO) {
                val link = pref.getString("rssFeed", getString(R.string.link_inicial))

                parser.getChannel(link!!)
            }

            binding.articlesRecycler.adapter = Adapter(channel.articles, this@MainActivity)
            binding.articlesRecycler.layoutManager = LinearLayoutManager(this@MainActivity)
        }

    }

    override fun onClick(episodio: Article) {
        val intent = Intent(this, EpisodeDetailActivity::class.java)

        intent.putExtra(EXTRA_DESCRIPTION, episodio.description)
        intent.putExtra(EXTRA_LINK, episodio.link)

        startActivity(intent)
    }

}